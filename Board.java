public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){
		this.tictactoeBoard = new Square[3][3];
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				this.tictactoeBoard[i][j] = Square.BLANK;
			}
		}
    }
	
	public String toString(){
        String string = "  0 1 2\n";
		for (int i = 0; i < 3; i++){
            string += i + " ";
			for (int j = 0; j < 3; j++){
				string += this.tictactoeBoard[i][j] + " ";
			}
			string += "\n";
		}
        return string;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
        boolean isPlace = true;
		if (row > 2 || col > 2 || row < 0 || col < 0){
			isPlace = false;
		}
		else if (this.tictactoeBoard[(row)][(col)] == Square.BLANK){
			this.tictactoeBoard[row][col] = playerToken;
		}
        else{
            isPlace = false;
        }
        return isPlace;
	}

    public boolean checkIfFull(){
        boolean isFull = true;
        for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
                if(this.tictactoeBoard[i][j] == Square.BLANK){
                    isFull = false;
                }
            }
        }
        return isFull;
    }

    private boolean checkIfWinningHorizontal(Square playerToken){
        boolean isWinHorizontal = false;
        for (int i = 0; i < 3; i++){
			int rowNum = 0;
            if (this.tictactoeBoard[rowNum][i] == playerToken){
				rowNum++;
                if (this.tictactoeBoard[rowNum][i] == playerToken){
					rowNum++;
                    if (this.tictactoeBoard[rowNum][i] == playerToken){
                        isWinHorizontal = true;
                    }
                }
            }
        }
        return isWinHorizontal;
    }
    
    private boolean checkIfWinningVertical(Square playerToken){
        boolean isWinVertical = false;
        for (int i = 0; i < 3; i++){
			int colNum = 0;
            if (this.tictactoeBoard[i][colNum] == playerToken){
				colNum++;
                if (this.tictactoeBoard[i][colNum] == playerToken){
					colNum++;
                    if (this.tictactoeBoard[i][colNum] == playerToken){
                        isWinVertical = true;
                    }
                }
            }
        }
        return isWinVertical;
    }
    
    public boolean checkIfWinningDiagonal(Square playerToken){
        boolean isWinDiagonal = false;
		int diagNum = 0; //Since the row and col were going to be the same number, I merged them both to make it easier.
        if (this.tictactoeBoard[diagNum][diagNum] == playerToken){ // This if block checks if the player won like this --->  \.
			diagNum++;
            if (this.tictactoeBoard[diagNum][diagNum] == playerToken){
				diagNum++;
                if (this.tictactoeBoard[diagNum][diagNum] == playerToken){
                    isWinDiagonal = true;
                }
            }
        }
		int row = 2;
		int col = 0;
        if (this.tictactoeBoard[row][col] == playerToken){ //This if block checks if the player won this way this time --->  /.
            row--;
			col++;
			if(this.tictactoeBoard[row][col] == playerToken){
				row--;
				col++;
                if(this.tictactoeBoard[row][col] == playerToken){
                    isWinDiagonal = true;
                }
            }
        }
        return isWinDiagonal;
    }
    
    public boolean checkIfWinning(Square playerToken){
        boolean isWinGame = false;
        if (checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true || checkIfWinningDiagonal(playerToken) == true){
            isWinGame = true;
        }
        return isWinGame;
    }
}









