import java.util.Scanner;

public class TicTacToeGame{
    
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;
        int row = 0;
        int column = 0;
        boolean isTokenValid = false;
        String ifContinue = "";
        boolean isContinue = true;
        int p1WinCount = 0; //player 1 win count
        int p2WinCount = 0; //player 2 win count
        
        System.out.println("Welcome, to the TicTacToe game!");
        System.out.println("Player's 1 token: " + Square.X);
        System.out.println("Player's 2 token: " + Square.O);
        System.out.println("Note: When placing your token, it's row first, then column.");
        System.out.println();
        
        while (isContinue == true){ //This is the bonus, the loop continues until the player doesn't want to play anymore.
            gameOver = false; //Put back gameOver to false, so we can play again.
            Board board = new Board(); //makes a new board for every new game.
            
            while (gameOver == false){ //This loops until one player loses, maaking gameOver to be true.
                System.out.println(board);

                if (player == 1){
                    playerToken = Square.X;
                }
                else{
                    playerToken = Square.O;
                }

                System.out.println("Player " + player + ": it's your turn! Where do you want to place your token?");
                row = scan.nextInt();
                column = scan.nextInt();

                isTokenValid = board.placeToken(row, column, playerToken);
                while (isTokenValid == false){
                    System.out.println("The row or column selected is unavailable, please try again.");
                    row = scan.nextInt();
                    column = scan.nextInt();
                    isTokenValid = board.placeToken(row, column, playerToken);
                }

                if (board.checkIfWinning(playerToken) == true){ //this if block comes first, or else the game will end, even if someone won at the last token being dropped.
                    System.out.println("Player " + player + " is the winner!");
                    gameOver = true;
                    
                    if(player == 1){
                        p1WinCount++;
                        player++; //if the game continues, player 2 will start next since player 1 won.
                    }
                    else{
                        p2WinCount++;
                        player--; //if the game continues, player 1 will start because they lost the game.
                    }             //If it's tied, the person who started last game will go first once more.
                }
                else if (board.checkIfFull() == true){
                    System.out.println("It's a tie!");
                    gameOver = true;
                }
                else{
                    player++;
                    if (player > 2){
                        player = 1;
                    }
                }
            }
            
            System.out.println("Do you wish to play another game? Y for Yes, N for No."); //Telling the user they only have the choice to type the letter Y or N, to prevent runtime errors.
            ifContinue = scan.next();

            if(ifContinue.equals("N")){
                isContinue = false;
            }
        }
        
        System.out.println("Player 1 has won " + p1WinCount + " times!");
        System.out.println("Player 2 has won " + p2WinCount + " times!");
        System.out.println("Thank you for playing!");
    }
}